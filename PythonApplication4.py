﻿
import math


def answer(x):
    temp=0
    i=0;
    while True:
        temp=pow(3,i)+pow(3,(i+1))
        if temp >= x:
            i+=1
            break
        i+=1
    list=[]
    while(i>=0):
        start = pow(3,i)/2
        start=round(start+0.1)
        while(start>0):
            if(x<=start+pow(3,i)-1):
                list.append('R')
                start=0
                i-=1
            elif(x<=start+pow(3,i)*2-1):
                list.append('L')
                start=0
                i-=1
            elif(x<=start+pow(3,i)*3-1):
                list.append('-')
                start=0
                i-=1
            else:
                start=start+pow(3,i)*3
            
    list=list.reverse()
    return list
answer(4)